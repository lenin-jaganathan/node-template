(function () {
    const nodemailer = require('nodemailer');
    const mailConfig = require('./config.json');

    const mailDetails = {
        service : 'gmail',
        auth : {
            type: 'OAuth2',
            user : mailConfig.mail.user,
            clientId: mailConfig.mail.clientId,
            clientSecret: mailConfig.mail.clientSecret,
            refreshToken: mailConfig.mail.refreshToken,
        }
    };
    transporter = nodemailer.createTransport(mailDetails);
    module.exports = transporter;
})();