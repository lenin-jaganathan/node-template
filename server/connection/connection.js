(function () {

    const mysql = require('mysql');
    const config = require('./config.json');
    const con = mysql.createConnection(config.prod);

    con.connect(function (err, data) {
        if(err){
            console.log("The following error occurred "+err.message);
        }
        else{
            console.log("connection successful");
        }
    });
    module.exports = con;
})();