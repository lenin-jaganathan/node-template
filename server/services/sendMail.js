(function () {
    const transporter  = require('../connection/mailConnection');

    module.exports.sendVerification = function (email, msg, callback) {
        let mailJson = {};
        mailJson.to = email;
        mailJson.from = '';
        mailJson.subject = '';
        mailJson.html = ''+msg;
        transporter.sendMail(mailJson,function (err, data) {
            callback(err,data);
        })
    }

})();