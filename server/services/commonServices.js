(function () {
    const connection = require('../connection/connection');
    const commonDao = require('../dao/commonDao');

    module.exports.beginTransaction = function (callback) {
        try{
            connection.beginTransaction(function (err) {
                callback(err);
            });
        }
        catch (err){
            callback(err);
        }
    };

    module.exports.rollBack = function (callback) {
        try{
            connection.rollback(function (err) {
                callback(err);
            });
        }
        catch (err){
            callback(err);
        }
    };

    module.exports.commit = function (callback) {
        try{
            connection.commit(function (err) {
                callback(err);
            });
        }
        catch(err){
            callback(err);
        }
    };   
})();