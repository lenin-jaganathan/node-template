(function () {
    const commonService = require('../services/commonServices');
    const responseHandler = require('./responseHandler');
    module.exports.rollBackHandler = function (res, err) {
        try {
            commonService.rollBack(function (rollerr) {
                if (rollerr) {
                    responseHandler.error(res, rollerr);
                }
                else {
                    responseHandler.error(res, err);
                }
            });
        }
        catch (err) {
            responseHandler.error(res, err);
        }
    };

    module.exports.commitHandler = function (res, data) {
        try {
            commonService.commit(function (commiterr) {
                if (commiterr) {
                    commonService.rollBack(function (rollerr) {
                        if (rollerr) {
                            console.log(1);
                            responseHandler.error(res, rollerr);
                        }
                        else {
                            console.log(2);
                            responseHandler.error(res, commiterr);
                        }
                    });
                }
                else {
                    responseHandler.response(res, data);
                }
            })
        }
        catch (err) {
            responseHandler.error(res, err);
        }
    };


})();