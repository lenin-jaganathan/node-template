(function () {
    let responseHandler = {
        error : function (res,err) {
            let statusCode = err.statusCode ? err.statusCode : 500;
            let stack = err.stack ? err.stack : "Unknown stack";
            let message = err.message ? err.message : "Unknown error";
            let errCode = err.code ? err.code : "Unknown code";
            let json  = {};
            json.statusCode = statusCode;
            json.stack = stack;
            json.message = message;
            json.errorCode = errCode;
            res.status(statusCode).json(json);
        },
        response : function (res, data) {
            let json = {};
            json.statusCode = 200;
            json.message = 'Success';
            json.data = data;
            res.status(200).json(json);
        }
    };
    module.exports = responseHandler;
})();