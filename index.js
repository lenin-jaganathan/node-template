(function () {
    const express = require('express');
    const config = require('./server/connection/config.json');
    const connection = require('./server/connection/connection');
    const mailer = require('./server/connection/mailConnection');

    const app = express();
    const bodyParser = require("body-parser");
    const cors = require("cors");


    app.use(cors('combined'));
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({
        extended: false
    }));

    const routes = require('./server/router')(app);

    process.on('SIGINT', function () {
        console.log('Server closed');
        process.exit(0);
    });

    app.listen(config.port);
})();